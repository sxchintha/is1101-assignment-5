#include <stdio.h>

#define PerformanceCost 500
#define AttendeeCost 3

	int ticketPrice;
	float prevProfit=0;
	float nowProfit;

void setTicketPrice(int tPrice)
{
	ticketPrice = tPrice;
}

int getTicketPrice()
{
	return ticketPrice;
}

int getAttendance()
{
	return 120-4*(ticketPrice-15);
}

float getIncome()
{
	return getAttendance()*ticketPrice;
}

float getExpence()
{
	return PerformanceCost+AttendeeCost*getAttendance();
}

float getProfit()
{
	return getIncome() - getExpence();
}

/**************************************
//get the maximum profit that owner can earn
float maxProfit()
{
	if(prevProfit=0)
	{
		prevProfit=getProfit();
	}
	else
	{
		setTicketPrice(0);
		prevProfit=getProfit();
	}
	

	for(int x=1;;x++)
	{
		setTicketPrice(x);
		nowProfit = getProfit();

		if(nowProfit<prevProfit)
		{
			return prevProfit;
		}
		else
		{
			prevProfit=nowProfit;
		}

	}
}
**************************************/

int main()
{
	printf("Relationship between profit and ticket price...\n\n");

	for(int n=5;n<=25;n=n+5)
	{
		setTicketPrice(n);
		int TicketPrice = getTicketPrice();
		float Profit = getProfit();

		printf("Ticket Price = Rs.%2d ----> Profit = Rs.%.2f\n", TicketPrice, Profit);
	}

/**************************************
	// get the maximum profit and the ticket value at that time
	float maximumProfit=maxProfit();
	int tPrice_on_max_prof=getTicketPrice()-1;
	printf("\nThe owner can make Rs.%.2f maximum profit at Rs.%d of ticket price.\n\n", maximumProfit, tPrice_on_max_prof);
**************************************/

/**************************************
	// to enter ticket price and get profit
	int t;
	scanf("%d", &t);
	setTicketPrice(t);
	printf("%.2f\n",getProfit() );
**************************************/

	return 0;
}
